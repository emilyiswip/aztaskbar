# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-21 13:54-0400\n"
"PO-Revision-Date: 2022-09-24 00:35+1000\n"
"Last-Translator: Ser82-png <asvmail.as@gmail.com>\n"
"Language-Team: \n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);\n"
"X-Generator: Poedit 3.0.1\n"

#: prefs.js:8
msgid "App Icons Taskbar"
msgstr "App Icons Taskbar"

#: prefs.js:9
msgid "Show running apps and favorites on the main panel"
msgstr "Показывать запущенные приложения и избранное на главной панели"

#: prefs.js:16
msgid "Settings"
msgstr "Настройки"

#: prefs.js:24
msgid "General"
msgstr "Общие"

#: prefs.js:29 prefs.js:80
msgid "Left"
msgstr "Слева"

#: prefs.js:30
msgid "Center"
msgstr "В центре"

#: prefs.js:31 prefs.js:81
msgid "Right"
msgstr "Справа"

#: prefs.js:33
msgid "Position in Panel"
msgstr "Расположение на панели"

#: prefs.js:56
msgid "Position Offset"
msgstr "Смещение положения"

#: prefs.js:57
msgid "Offset the position within the above selected box"
msgstr "Сместить положение в пределах диапазона выбранного выше"

#: prefs.js:89
msgid "Show Apps Button"
msgstr "Отображать кнопку «Показать приложения»"

#: prefs.js:106
msgid "Show Focused Window Title"
msgstr "Показывать заголовок окна, находящегося в фокусе"

#: prefs.js:120
msgid "Favorites"
msgstr "Избранное"

#: prefs.js:134
msgid "Isolate Workspaces"
msgstr "Изолировать рабочие пространства"

#: prefs.js:148
msgid "Isolate Monitors"
msgstr "Изолировать мониторы"

#: prefs.js:159
msgid "Panel"
msgstr "Панель"

#: prefs.js:167
msgid "Show Panels on All Monitors"
msgstr "Показывать панель на всех мониторах"

#: prefs.js:209
msgid "Panel Height"
msgstr "Высота панели"

#: prefs.js:223
msgid "App Icons"
msgstr "Значки приложений"

#: prefs.js:241
msgid "Icon Size"
msgstr "Размер значка"

#: prefs.js:261
msgid "Icon Desaturate Factor"
msgstr "Коэффициент обесцвечивания значка"

#: prefs.js:268
msgid "Regular"
msgstr "Обычные"

#: prefs.js:269
msgid "Symbolic"
msgstr "Символьные"

#: prefs.js:271
msgid "Icon Style"
msgstr "Стиль значков"

#: prefs.js:272
msgid "Icon themes may not have a symbolic icon for every app"
msgstr "Темы значков могут иметь символические значки не для всех приложений"

#: prefs.js:282 prefs.js:287
msgid "Indicator"
msgstr "Индикатор"

#: prefs.js:288
msgid "Multi-Dashes"
msgstr "Мульти-чёрточки"

#: prefs.js:290
msgid "Multi-Window Indicator Style"
msgstr "Стиль многооконного индикатора"

#: prefs.js:300
msgid "Top"
msgstr "Сверху"

#: prefs.js:301
msgid "Bottom"
msgstr "Снизу"

#: prefs.js:303
msgid "Indicator Location"
msgstr "Расположение индикатора"

#: prefs.js:324
msgid "Running Indicator Color"
msgstr "Цвет индикатора запущенного приложения"

#: prefs.js:343
msgid "Focused Indicator Color"
msgstr "Цвет индикатора приложения, находящегося в фокусе"

#: prefs.js:355
msgid "Actions"
msgstr "Действия"

#: prefs.js:362
msgid "Click Actions"
msgstr "Действия при щелчке мышью"

#: prefs.js:367
msgid "Toggle / Cycle"
msgstr "Переключение / По кругу"

#: prefs.js:368
msgid "Toggle / Cycle + Minimize"
msgstr "Переключение / По кругу + Свернуть"

#: prefs.js:369
msgid "Toggle / Preview"
msgstr "Переключение / Просмотр"

#: prefs.js:370
msgid "Cycle"
msgstr "По кругу"

#: prefs.js:377
msgid "Left Click"
msgstr "Щелчок левой кнопкой мыши"

#: prefs.js:378
msgid "Modify Left Click Action of Running App Icons"
msgstr ""
"Изменить действие щелчка левой кнопкой мыши на значках запущенных приложений"

#: prefs.js:388
msgid "Scroll Actions"
msgstr "Действия прокрутки"

#: prefs.js:393
msgid "Cycle Windows"
msgstr "Окна по кругу"

#: prefs.js:394
msgid "No Action"
msgstr "Нет действий"

#: prefs.js:396
msgid "Scroll Action"
msgstr "Действие прокрутки"

#: prefs.js:406
msgid "Hover Actions"
msgstr "Действия при наведении"

#: prefs.js:414
msgid "Tool-Tips"
msgstr "Подсказки"

#: prefs.js:436 prefs.js:468
msgid "Window Previews"
msgstr "Окна предварительного просмотра"

#: prefs.js:455
msgid "Window Preview Options"
msgstr "Параметры окна предварительного просмотра"

#: prefs.js:486
msgid "Show Window Previews Delay"
msgstr "Задержка при показе окна предварительного просмотра"

#: prefs.js:487
msgid "Time in ms to show the window preview"
msgstr "Время в мс для отображения окна предварительного просмотра"

#: prefs.js:507
msgid "Hide Window Previews Delay"
msgstr "Задержка до скрытия окна предварительного просмотра"

#: prefs.js:508
msgid "Time in ms to hide the window preview"
msgstr "Время в мс для скрытия окна предварительного просмотра"

#: prefs.js:515 prefs.js:523
msgid "Window Peeking"
msgstr "Быстрый просмотр окна"

#: prefs.js:524
msgid "Hovering a window preview will focus desired window"
msgstr "Наведение на окно предварительного просмотра выведет нужное окно"

#: prefs.js:548
msgid "Window Peeking Delay"
msgstr "Задержка при выводе быстрого просмотра окна"

#: prefs.js:549
msgid "Time in ms to trigger window peek"
msgstr "Время в мс для запуска быстрого просмотра окна"

#: prefs.js:569
msgid "Window Peeking Opacity"
msgstr "Непрозрачность при быстром просмотре окна"

#: prefs.js:570
msgid "Opacity of non-focused windows during a window peek"
msgstr "Непрозрачность несфокусированных окон во время быстрого просмотра"

#: prefs.js:582
msgid "About"
msgstr "О приложении"

#: prefs.js:626
msgid "Version"
msgstr "Версия"

#: prefs.js:635
msgid "Git Commit"
msgstr "Git Commit"

#: prefs.js:644
msgid "GNOME Version"
msgstr "Версия GNOME"

#: prefs.js:652
msgid "OS"
msgstr "ОС"

#: prefs.js:674
msgid "Session Type"
msgstr "Тип сессии"

#: appIcon.js:206
msgid "Show Applications"
msgstr "Показывать приложения"
